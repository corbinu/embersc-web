# Ember-SC Web App

Ember SC Web App

## Dependencies
* [Node.js](http://nodejs.org) or [NVM](https://github.com/creationix/nvm)
* `npm install -g ember-cli bower phantomjs`

## Installation

* `git clone https://bitbucket.org/corbinu/embersc-web` this repository
* `cd embersc-web`
* `bower install && npm install`

## Running / Development

* `ember server --proxy=http://localhost:3000`
* Visit your app at http://localhost:4200.

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Deploy with a web server like Nginx

## Further Reading / Useful Links

* ember: http://emberjs.com/
* ember-cli: http://www.ember-cli.com/
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

