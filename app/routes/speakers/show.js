import Ember from 'ember';

export default Ember.Route.extend({

    actions: {
        remove: function () {
            this.controller.get('model').destroyRecord();
            this.transitionTo('speakers.index');
        }
    }
});