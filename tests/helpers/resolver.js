import Resolver from 'ember/resolver';

var resolver = Resolver.create();

resolver.namespace = {
  modulePrefix: 'embersc-web'
};

export default resolver;
