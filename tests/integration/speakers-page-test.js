import Ember from "ember";
import { test } from 'ember-qunit';
import startApp from '../helpers/start-app';
import testOnSameRow from '../helpers/test-on-same-row';

var App, server;

module('Integration - Speaker Page', {
    setup: function () {
        App = startApp();
    },
    teardown: function () {
        Ember.run(App, App.destroy);
    }
});

test('Should allow navigation to the speakers page from the landing page', function() {
    expect(1);
    visit('/').then(function() {
        click('a:contains("Speakers")').then(function() {
            equal(find('h3').text(), 'Speakers');
        });
    });
});

test('Should list all speakers and number of presentations', function () {
    expect(3);
    visit('/speakers').then(function () {
        testOnSameRow('Bugs Bunny', '2');
        testOnSameRow('Wile E. Coyote', '1');
        testOnSameRow('Yosemite Sam', '3');
    });
});

test('Should be able to navigate to a speaker page', function() {
    expect(1);
    visit('/speakers').then(function() {
        click('a:contains("Bugs Bunny")').then(function() {
            equal(find('h4').text(), 'Bugs Bunny');
        });
    });
});

test('Should be able visit a speaker page', function() {
    expect(1);
    visit('/speakers/1').then(function() {
        equal(find('h4').text(), 'Bugs Bunny');
    });
});

test('Should list all presentations for a speaker', function() {
    expect(2);
    visit('/speakers/1').then(function() {
        equal(find('li:contains("What\'s up with Docs?")').length, 1);
        equal(find('li:contains("Of course, you know, this means war.")').length, 1);
    });
});


test('Should see edit button to edit a speaker', function () {
    expect(1);
    visit('/speakers/1').then(function() {
        ok(find('a').hasClass('edit-speaker'));
    });
});

test('Should be able to visit a speaker edit page', function () {
    expect(1);
    visit('/speakers/1');
    click('a.edit-speaker');
    andThen(function() {
        notEqual(find('input.speaker-name'), undefined);
    });
});

test("Should see a button or link to commit the edit change", function() {
    expect(1);
    visit('/speakers/1/edit');
    andThen(function() {
        ok(find('button').hasClass('commit-speaker-change'));
    });
});

test("Change the name of 'Bugs Bunny' to 'Silly Wabbit'", function() {
    expect(1);
    visit('/speakers/1/edit');
    andThen(function() {
        fillIn('input.speaker-name', 'Silly Wabbit');
        click(findWithAssert('button').hasClass('commit-speaker-change'));
        andThen(function() {
            equal(findWithAssert('input.speaker-name').val(), 'Silly Wabbit');
        });
    });
});

test("Can see a link to create a new speaker", function() {
    expect(1);
    visit('/speakers');
    andThen(function() {
        ok(find('a').hasClass('create-speaker'));
    });
});

test("Can navigate to a page to create a speaker", function() {
    expect(1);
    visit('/speakers');
    click(find('a.create-speaker'));
    andThen(function() {
        ok(find('button').hasClass('commit-speaker-creation'));
    });
});

test("Can see a button to delete an existing speaker", function() {
    visit('/speakers/1');
    andThen(function() {
        ok(find('button').hasClass('delete-speaker'));
    });
});

test("Can delete an existing speaker", function() {
    visit('/speakers/1');
    click(find('button.delete-speaker'));
    andThen(function() {
        equal(currentRouteName(), 'speakers.index');
    });
});