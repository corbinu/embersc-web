import Ember from "ember";
import { test } from 'ember-qunit';
import startApp from '../helpers/start-app';

var App;

module('Integration - About Page', {
    setup: function () {
        App = startApp();
    },
    teardown: function () {
        Ember.run(App, App.destroy);
    }
});

test('Should navigate to the About page', function() {
    expect(1);
    visit('/').then(function() {
        click("a:contains('About')").then(function() {
            equal(find('h3').text(), 'About');
        });
    });
});