import Ember from "ember";
import { test } from 'ember-qunit';
import startApp from '../helpers/start-app';
import testOnSameRow from '../helpers/test-on-same-row';

var App, server;

module('Integration - Presentation Page', {
    setup: function () {
        App = startApp();
    },
    teardown: function () {
        Ember.run(App, App.destroy);
    }
});

test('Should allow navigation to a list of presentations page from the landing page', function () {
    expect(1);
    visit('/');
    click("a:contains('Presentations')");
    andThen(function () {
        equal(find('h3').text(), 'Presentations');
    });
});

test('Should list all presentations and the speaker for each presentation', function () {
    expect(6);
    visit('/presentations').then(function () {
        testOnSameRow('What\'s up with Docs?', 'Bugs Bunny');
        testOnSameRow('Of course, you know, this means war.', 'Bugs Bunny');
        testOnSameRow('Getting the most from the Acme catalog.', 'Wile E. Coyote');
        testOnSameRow('Shaaaad up!', 'Yosemite Sam');
        testOnSameRow('Ah hates rabbits.', 'Yosemite Sam');
        testOnSameRow('The Great horni-todes', 'Yosemite Sam');
    });
});

test('Should allow navigation to an edit page for the presentation from the presentation list', function() {
    expect(2);
    visit('/presentations');
    click("a:contains('Shaaaad up!')");
    andThen(function() {
        equal(currentURL(), '/presentations/4/edit');
        equal(find('input.presentation-title').val(), 'Shaaaad up!');
    });
});

test('Should go to speaker\'s show page after submitting presentation edit', function() {
    expect(3);
    visit('/presentations/4/edit');
    fillIn('input.presentation-title', 'Be Quiet!');
    click('button.commit-presentation-change');
    andThen(function() {
        equal(currentURL(), '/speakers/3');
        ok("h4:contains('Yosemite Sam')");
        ok("li:contains('Be Quiet!')");
    });
});